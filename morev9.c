#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <termios.h>
#include <string.h>

#define LINELEN 512

void do_more(FILE *, char* argv);
int get_input(FILE *, int);
void set_input_mode(void);
void reset_input_mode(void);
int findString(FILE *);

int total_lines=0;
int total_lines_read=0;
struct termios saved_attr;

int main(int argc, char* argv[])
{

	int i=0;
	if(argc==1){
		do_more(stdin, argv[0]);
	}
	FILE * fp;
	while (++i < argc)
	{
		fp=fopen(argv[i], "r");
		if (fp==NULL)
		{
			perror("can't open file");
			exit(1);
		}
		do_more(fp, argv[i]);
		fclose(fp);
	}

	return 0;
}

void do_more(FILE * fp, char* argv)
{
	//get env variable LINES
	struct winsize max;
	ioctl(0, TIOCGWINSZ, &max);

	//Count total lines in file
	char buffer[LINELEN];
	while(fgets(buffer,512,fp)!= NULL)
	{
		total_lines++;
	}

	fseek(fp, 0, SEEK_SET);
	int num_of_lines=0;
	int rv;
	int didSearchCallFlag=0;
	

	FILE * fp_tty = fopen("/dev//tty", "r");
	while (fgets(buffer, LINELEN, fp)){
		didSearchCallFlag=0;
		fputs(buffer,stdout);
		num_of_lines++;
		total_lines_read++;
		if (num_of_lines >= max.ws_row-1){
takeInput:		rv=get_input(fp_tty, didSearchCallFlag);
			if (rv==0){ //pressed q
				printf("\033[2K \033[1G");
				break;
			}
			else if (rv==1) //pressed space bar
			{
				printf("\033[2K \033[1G");
				num_of_lines =0;
			}
			else if (rv==2) // user pressed enter
			{
				printf("\033[2K \033[1G");
				num_of_lines -= 1;
			}
			else if (rv==3) // user pressed /search
			{
				int retval;
				printf("\033[2K \033[1G");
				retval = findString(fp);
				if (retval==-1)
				{
					num_of_lines=max.ws_row-1;
					didSearchCallFlag=1;
					goto takeInput;

				}
				else if (retval ==1)
				{
					num_of_lines= 0;
				}	
			}
			else if (rv==4) // user pressed v .. open vim
			{
				pid_t pid;
				pid=fork();

				if (pid==0) // child process
				{
					execlp("vim", "myvim", "+1" ,argv, (char*)NULL);
				}
				else
				{
					wait(NULL);
					didSearchCallFlag=1;
					goto takeInput;
				}
			}
			else if (rv==5) //invalid character
			{
				printf("\033[2K \033[1G");
				break;
			}
		}
	}
}

int get_input(FILE * cmdstream, int didSearchCall)
{
	int percentage;
	percentage= (total_lines_read/(float)total_lines)*100;

	int c;
	if (!didSearchCall)
		printf("\033[7m --more--(%d%%) \033[m",percentage);
	
	set_input_mode();
	c=getc(cmdstream);

	reset_input_mode();
	if(c=='q')
		return 0;
	if(c==' ')
		return 1;
	if(c=='\n')
		return 2;
	if(c=='/')
		return 3;
	if(c=='v')
		return 4;
	return 5;
}

void reset_input_mode(void)
{
	tcsetattr (0, TCSANOW, &saved_attr);
}

void set_input_mode(void)
{
	struct termios tattr;

	//save the attributes before changin to reset them later
	tcgetattr(0, &saved_attr);

	//set non canonical and non echo mode
	tcgetattr(0,&tattr);
	tattr.c_lflag &= ~(ICANON|ECHO);

	tcsetattr(0, TCSAFLUSH, &tattr);
}

int findString(FILE* fp)
{
	char key[512];
	int flag=0;
	int charsRead=0;
	int totalCharsRead=0;
	printf("/");	
	char buffer[LINELEN];
	fgets(key,512,stdin);

	while (fgets(buffer,512,fp) !=NULL)
	{
		charsRead= strlen(buffer);
		totalCharsRead += charsRead;
		if (strstr (buffer,key) != NULL)
		{
			printf("skipping...\n");
			fseek(fp, -1*charsRead, SEEK_CUR);
			flag=1;
			return 1;
		}
	}

	if (!flag)
	{
		fseek(fp, -1*totalCharsRead, SEEK_CUR);
		printf("\033[2K \033[1G");
		printf("\033[7m Pattern not found \033[m");
		return -1;
	}	
}
